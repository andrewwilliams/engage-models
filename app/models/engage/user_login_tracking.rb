class Engage::UserLoginTracking < ActiveRecord::Base
  set_table_name 'engage_user_login_trackings'
  belongs_to :user, :class_name => 'Engage::User'

  validates :visiting_domain, :presence => true
end
