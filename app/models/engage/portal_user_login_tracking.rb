class Engage::PortalUserLoginTracking < ActiveRecord::Base
  set_table_name 'portal_user_login_trackings'
  belongs_to :user, :class_name => 'User'

  validates :visiting_domain, :presence => true
end
