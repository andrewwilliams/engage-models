class Engage::Dealer < ActiveRecord::Base
  set_table_name 'engage_dealers'

  has_many :users, :class_name => 'Engage::User'

  validates :name, :domain, :presence => true

  attr_accessible :name, :domain

  def self.get_domain_dealers(domain)
    case domain
    when Rails.application.config.alfa_domain
      find_domain = "Alfa"
    when Rails.application.config.fiat_domain
      find_domain = "Fiat"
    when Rails.application.config.land_rover_domain
      find_domain = "Land Rover"
    else
      find_domain = "Jaguar"
    end
  
    Engage::Dealer.where(domain: find_domain)
  end
end
