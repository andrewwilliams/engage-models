class Engage::User < ActiveRecord::Base
  include ActionView::Helpers::DateHelper
  set_table_name 'engage_users'

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :timeoutable

  has_many :user_login_trackings, :class_name => 'Engage::UserLoginTracking'
  belongs_to :engage_dealer, :class_name => 'Engage::Dealer'

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :name, :password, :password_confirmation, :dealer_code, :engage_dealer_id, :domain, :access_revoked

  validates :dealer_code, :engage_dealer_id, :email, :name, :presence => true
  validates_uniqueness_of :email, :scope => :domain
  validates_length_of :dealer_code, :minimum => 5, :maximum => 6
  validates_format_of :email, with: /\A[^@]+@[^@]+\z/
  validates_format_of :email, without: /(?:bt.com|yahoo|hotmail|gmail|aol.com|msn.com|btinternet.com|virgin.net)/i, :message => 'domain is not allowed'

  before_create :setup_password

  def active_for_authentication?
    super && access_is_not_revoked?
  end

  def record_user_tracking(domain)
    tracking = Engage::UserLoginTracking.new
    tracking.visiting_domain = domain
    user_login_trackings << tracking
  end
  
  def record_last_page_time_stamp(timestamp)
    self.last_page_time_stamp = timestamp
  end
  
  def last_login_period
    return 0 if self.last_page_time_stamp.nil?
    
    distance_of_time_in_words(self.last_page_time_stamp, self.current_sign_in_at, true)
  end

  private

  def access_is_not_revoked?
    !access_revoked
  end

  def setup_password
    first_user = Engage::User.first
    if first_user != nil 
      self.encrypted_password = first_user.encrypted_password
    else
      self.password = 'Engage'
    end

  end
end
