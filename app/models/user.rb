class User < ActiveRecord::Base
  include ActionView::Helpers::DateHelper
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
          :rememberable, :trackable, :validatable, :timeoutable

  has_many :user_login_trackings, :class_name => 'Engage::PortalUserLoginTracking'

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :first_name, :last_name, :brand_ids

  validates :first_name, :last_name, presence: true


  has_and_belongs_to_many :brands
  has_many :last_viewed_items, dependent: :destroy

  before_create :setup_password

  def password_required?
    new_record? ? false : super
  end

  def can_access_multiple_brands?
    return brands.length > 1
  end

  def record_user_tracking(domain)
    tracking = Engage::PortalUserLoginTracking.new
    tracking.visiting_domain = domain
    user_login_trackings << tracking
  end
  
  def record_last_page_time_stamp(timestamp)
    self.last_page_time_stamp = timestamp
  end
  
  def last_login_period
    return 0 if self.last_page_time_stamp.nil?
    
    distance_of_time_in_words(self.last_page_time_stamp, self.current_sign_in_at, true)
  end



  private

  def setup_password
    first_user = User.first
    if first_user != nil 
      self.encrypted_password = first_user.encrypted_password
    else
      self.password = 'default'
    end

  end
end
