$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "engage_models/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "engage_models"
  s.version     = EngageModels::VERSION
  s.authors     = ["John Polling"]
  s.email       = ["john@theled.co.uk"]
  s.summary     = "Models for the Engage work"
  s.description = "Models for the Engage work to be used in a couple of projects"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 3.2.6"
  s.add_dependency "devise"
  # s.add_dependency "jquery-rails"

end
